resource "aws_key_pair" "mykeypairother" {
  key_name   = "mykeypairother"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}


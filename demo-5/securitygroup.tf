data "aws_ip_ranges" "weast" {
  regions  = ["us-west-1"]
  services = ["ec2"]
}

resource "aws_security_group" "weast" {
  name = "west-ssl-group"

  ingress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
    cidr_blocks = data.aws_ip_ranges.weast.cidr_blocks
  }
  tags = {
    CreateDate = data.aws_ip_ranges.weast.create_date
    SyncToken  = data.aws_ip_ranges.weast.sync_token
  }
}


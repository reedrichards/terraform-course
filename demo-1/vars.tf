variable "AWS_ACCESS_KEY" {
}

variable "AWS_SECRET_KEY" {
}

variable "AWS_REGION" {
  default = "us-east-2"
}


variable "AMIS" {
  type = map(string)
  default = {
    us-east-2 = "ami-0d5d9d301c853a04a"
    us-east-2 = "ami-04b9e92b5572fa0d1"
    us-west-1 = "ami-0dd655843c87b6930"
  }
}

